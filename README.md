# snt - simple/suckless new tab (wip)

![Catppuccin](./preview/1.png)
![Nord](./preview/2.png)

[Live preview](https://vnpower.codeberg.page/snt)

## Features
- Fast
- Lightweight (small codebase, 234 lines of code in v1.0)
- Hackable
- etc...

## Usage
#### Locally:
Currently, only Chromium will be able to use this locally (i had problems with Firefox's extensions bullshit.)

1. Clone the repo
2. Go to `Extensions` and enable `Developer Mode` (top right)
3. `Load unpacked` (top left)
4. Choose the folder you just cloned
5. Enable the extension

This is much more comfortable than the online method, you will be able to use this offline, customization is instant.
#### Online:
1. Fork this repo
2. Host your forked repo somewhere (if you want to use GitHub pages, fork this to GitHub, or Codeberg, your choice)
3. Follow [this guide](https://docs.github.com/en/pages/getting-started-with-github-pages/creating-a-github-pages-site) to host your repo on GitHub, or [this guide](https://codeberg.page/) for Codeberg.
4. 
- Use [Custom New Tab Page](https://addons.mozilla.org/en-US/firefox/addon/custom-new-tab-page/?src=search) for Firefox and enable "Force links to open in the top frame (experimental)" in the extension's preferences page
- Use [Custom New Tab URL](https://chrome.google.com/webstore/detail/custom-new-tab-url/mmjbdbjnoablegbkcklggeknkfcjkjia) for Chromium

If you had any problems, feel free to contact me on Discord (VnPower#5919). 

## Customization
Customization is done directly in `config.js`. I also put some comments in there so you can understand what stuff do.

#### Buttons
Currently, only SVGs are allowed for icons.
```js
  buttons: [
    {
      name: "Discord",
      icon: `
        <svg fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" width="50px" height="50px">
          <path
            d="M 18.90625 7 ..." />
        </svg>
      `,
      link: "https://discord.com/app"
    },
    {...}
```

## Credits
- [Bento](https://github.com/migueravila/Bento) - Inspiration
- [Icons8](https://icons8.com/) - Icons
- [Catppuccin](https://github.com/catppuccin/catppuccin), [Nord](https://nordtheme.com) - Color scheme
- wallhaven - Forest wallpaper
- 甘城なつき - Nachoneko's illust

## License
[GPL-3.0](./LICENSE)
