let buttonContainer = document.querySelector("#links");

for (const button of CONFIG.buttons) {
  let item = "";
  // If SVG
  if (! button.icon.trim().startsWith("<svg") ) {
    item = `
    <a title="${button.name}" href="${button.link}" class="button">
      <img src="${button.icon}" alt="${button.name}">
    </a>
    `;
  } else {
    // If IMG
    item = `
    <a title="${button.name}" href="${button.link}" class="button">
      ${button.icon}
    </a>
    `;
  }


  const position = "beforeend";

  links.insertAdjacentHTML(position, item); 
}
